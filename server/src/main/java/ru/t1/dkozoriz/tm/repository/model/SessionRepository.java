package ru.t1.dkozoriz.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.model.ISessionRepository;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends UserOwnedRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<Session> getClazz() {
        return Session.class;
    }

}