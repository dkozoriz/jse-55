package ru.t1.dkozoriz.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.dto.IAbstractDtoRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.IAbstractDtoService;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractDtoService<T extends AbstractModelDto> implements IAbstractDtoService<T> {

    @NotNull
    private final IServiceLocator serviceLocator;

    protected AbstractDtoService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected abstract String getName();

    @NotNull
    protected IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return serviceLocator.getConnectionService().getEntityManager();
    }

    @NotNull
    protected abstract IAbstractDtoRepository<T> getRepository(@NotNull final EntityManager entityManager);

    @Override
    @Nullable
    public T add(@Nullable final T model) {
        if (model == null) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void update(@NotNull final T model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<T> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final T model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(id);
        if (model == null) return;
        remove(model);
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable final T model = findByIndex(index);
        if (model == null) return;
        remove(model);
    }

    @Override
    @Nullable
    public T findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            return repository.findById(id.trim());
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public T findByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            return repository.findByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IAbstractDtoRepository<T> repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}