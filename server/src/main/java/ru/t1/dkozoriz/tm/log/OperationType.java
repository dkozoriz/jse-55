package ru.t1.dkozoriz.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}