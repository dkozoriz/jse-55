package ru.t1.dkozoriz.tm.exception.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.exception.AbstractException;

public abstract class AbstractServerException extends AbstractException {

    public AbstractServerException() {
    }

    public AbstractServerException(@NotNull final String message) {
        super(message);
    }

    public AbstractServerException(@NotNull final String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractServerException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractServerException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
