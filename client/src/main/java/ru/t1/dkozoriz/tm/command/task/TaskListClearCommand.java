package ru.t1.dkozoriz.tm.command.task;

import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskListClearRequest;

@Component
public final class TaskListClearCommand extends AbstractTaskCommand {

    public TaskListClearCommand() {
        super("task-clear", "delete all tasks.");
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        taskEndpoint.taskListClear(new TaskListClearRequest(getToken()));
    }

}