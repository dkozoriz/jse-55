package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @Autowired
    protected ISystemEndpoint systemEndpoint;

    public AbstractSystemCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public AbstractSystemCommand(@NotNull String name, @Nullable String description, @Nullable String argument) {
        super(name, description, argument);
    }

    public Role[] getRoles() {
        return null;
    }

}