package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public TaskStartByIndexCommand() {
        super("task-start-by-index", "start task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        taskEndpoint.taskStartByIndex(new TaskStartByIndexRequest(getToken(), index));
    }

}