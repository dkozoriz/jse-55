package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    public ProjectShowByIndexCommand() {
        super("project-show-by-index", "show project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectDto project =
                projectEndpoint.projectShowByIndex(new ProjectShowByIndexRequest(getToken(), index)).getProject();
        showProject(project);
    }

}