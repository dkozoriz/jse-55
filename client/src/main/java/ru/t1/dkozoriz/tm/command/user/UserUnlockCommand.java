package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserLockRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserUnlockRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserUnlockCommand extends AbstractUserCommand {

    public UserUnlockCommand() {
        super("user-unlock", "user unlock.");
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.userUnlock(new UserUnlockRequest(getToken(), login));
    }
}
