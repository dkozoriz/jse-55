package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public TaskCompleteByIndexCommand() {
        super("task-complete-by-index", "complete task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        taskEndpoint.taskCompleteByIndex(new TaskCompleteByIndexRequest(getToken(), index));
    }

}