package ru.t1.dkozoriz.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.service.ICommandService;
import ru.t1.dkozoriz.tm.api.service.ILoggerService;
import ru.t1.dkozoriz.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @Autowired
    @NotNull
    private ILoggerService loggerService;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommands();
        commands.forEach(command -> this.commands.add(command.getName()));
        executorService.scheduleWithFixedDelay(this::process, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void process() {
        for (@NotNull final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@NotNull final Exception e) {
                    loggerService.error(e);
                }
            }
        }
    }

}