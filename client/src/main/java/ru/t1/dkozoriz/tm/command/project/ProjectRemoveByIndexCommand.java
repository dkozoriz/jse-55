package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public ProjectRemoveByIndexCommand() {
        super("project-remove-by-index", "remove project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        projectEndpoint.projectRemoveByIndex(new ProjectRemoveByIndexRequest(getToken(), index));
    }

}