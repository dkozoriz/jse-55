package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectStartByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public ProjectStartByIndexCommand() {
        super("project-start-by-index", "start project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        projectEndpoint.projectStartByIndex(new ProjectStartByIndexRequest(getToken(), index));
    }

}