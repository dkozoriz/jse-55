package ru.t1.dkozoriz.tm.command.system;

import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public ApplicationAboutCommand() {
        super("about", "show developer info.", "-a");
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + systemEndpoint.getAbout(new ServerAboutRequest()).getName());
        System.out.println("E-mail: " + systemEndpoint.getAbout(new ServerAboutRequest()).getEmail() + "\n");

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + systemEndpoint.getAbout(new ServerAboutRequest()).getApplicationName() + "\n");

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + systemEndpoint.getAbout(new ServerAboutRequest()).getBranch());
        System.out.println("COMMIT ID: " + systemEndpoint.getAbout(new ServerAboutRequest()).getCommitId());
        System.out.println("COMMITTER NAME: " + systemEndpoint.getAbout(new ServerAboutRequest()).getCommitterName());
        System.out.println("COMMITTER EMAIL: " + systemEndpoint.getAbout(new ServerAboutRequest()).getCommitterEmail());
        System.out.println("MESSAGE: " + systemEndpoint.getAbout(new ServerAboutRequest()).getMessage());
        System.out.println("TIME: " + systemEndpoint.getAbout(new ServerAboutRequest()).getTime());
    }

}