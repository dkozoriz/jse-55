package ru.t1.dkozoriz.tm.command.project;

import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectListClearRequest;

@Component
public final class ProjectListClearCommand extends AbstractProjectCommand {

    public ProjectListClearCommand() {
        super("project-clear", "delete all projects.");
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        projectEndpoint.projectListClear(new ProjectListClearRequest(getToken()));
    }

}