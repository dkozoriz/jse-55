package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    public TaskCompleteByIdCommand() {
        super("task-complete-by-id", "complete task by id.");
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        taskEndpoint.taskCompleteById(new TaskCompleteByIdRequest(getToken(), id));
    }

}