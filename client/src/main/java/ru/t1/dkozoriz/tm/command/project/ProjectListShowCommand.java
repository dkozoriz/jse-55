package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectShowListRequest;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListShowCommand extends AbstractProjectCommand {

    public ProjectListShowCommand() {
        super("project-list", "show project list.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @Nullable final List<ProjectDto> projects =
                projectEndpoint.projectList(new ProjectShowListRequest(getToken(), sort)).getProjectList();
        int index = 1;
        for (final ProjectDto project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}