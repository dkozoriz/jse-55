package ru.t1.dkozoriz.tm.command.system;

import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dkozoriz.tm.dto.request.system.ServerVersionRequest;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public ApplicationVersionCommand() {
        super("version", "show version info.", "-v");
    }

    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("version: " + systemEndpoint.getVersion(new ServerVersionRequest()).getVersion());
    }

}