package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserLockRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public UserUpdateProfileCommand() {
        super("update-user-profile", "update profile of current user.");
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        userEndpoint.userUpdateProfile(new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName));
    }

}