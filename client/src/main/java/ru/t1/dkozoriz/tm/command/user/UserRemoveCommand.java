package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserRemoveRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserRemoveCommand extends AbstractUserCommand {

    public UserRemoveCommand() {
        super("user-remove", "user remove.");
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.userRemove(new UserRemoveRequest(getToken(), login));
    }

}