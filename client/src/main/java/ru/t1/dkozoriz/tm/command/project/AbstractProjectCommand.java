package ru.t1.dkozoriz.tm.command.project;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Autowired
    protected IProjectEndpoint projectEndpoint;

    public AbstractProjectCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final ProjectDto project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}