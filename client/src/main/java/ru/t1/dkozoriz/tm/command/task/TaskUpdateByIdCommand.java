package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public TaskUpdateByIdCommand() {
        super("task-update-by-id", "update task by id.");
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        taskEndpoint.taskUpdateById(new TaskUpdateByIdRequest(getToken(), id, name, description));
    }

}